package com.wwmxd.service.impl;

import com.wwmxd.entity.Demo;
import com.wwmxd.dao.DemoDao;
import com.wwmxd.service.DemoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-12 10:59:07
 */
@Service
public class DemoServiceImpl extends ServiceImpl<DemoDao, Demo> implements DemoService  {

}
